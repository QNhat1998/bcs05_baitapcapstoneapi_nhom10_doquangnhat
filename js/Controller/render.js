const getElement = id => document.getElementById(id)
const renderPhoneItem = (products) => {
    let strProduct = ''
    products.map((product, index) => {
        let { name, img, id, desc, price } = product
        strProduct += `
        <div class="col-3">
            <div class="card product border-0 overflow-hidden">
                <img src="${img}"
                    class="card-img-top" alt="...">
                <div class="card-body text-light">
                    <h5 class="card-title">${name}</h5>
                    <h3 class="card-title">${price.toLocaleString()} vnđ</h3>
                    <p class="card-text">${desc}</p>
                </div>
                <div class='card-footer'>
                    <button onclick='addCart("${id}")' class="btn btn-primary">Thêm vào giỏ</button>
                </div>
            </div>
        </div>
        `
    })
    return strProduct
}

const renderTabPane = (length, products) => {
    let strTabPane = ''
    for (let index = 0; index < length; index += 4) {
        if (index === 0) {
            strTabPane += `
            <div class="tab-pane fade show active" id="tab0" role="tabpanel" aria-labelledby="tab0-tab">
                <div id='page0' class='row'>${renderPhoneItem(products.slice(index, 4))}</div>
            </div>
            `
        }
        else {
            strTabPane += `
            <div class="tab-pane fade" id="tab${index}" role="tabpanel" aria-labelledby="tab${index}-tab">
                <div id='page${index}' class='row'>${renderPhoneItem(products.slice(index, index + 4))}</div>
            </div>
            `
        }

    }
    getElement('myTabContent').innerHTML = strTabPane
}

const renderNavItem = (length) => {
    let strNavItem = '', page = 1
    for (let index = 0; index < length; index += 4) {
        if (index === 0) {
            strNavItem += `
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="tab0-tab" data-bs-toggle="tab" data-bs-target="#tab0"
                    type="button" role="tab" aria-controls="tab0" aria-selected="true">${page}</button>
            </li>
            `
        }
        else {
            strNavItem += `
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="tab${index}-tab" data-bs-toggle="tab" data-bs-target="#tab${index}"
                    type="button" role="tab" aria-controls="tab${index}" aria-selected="fakse">${++page}</button>
            </li>
        `
        }
    }
    getElement('myTab').innerHTML = strNavItem
}

export const renderProduct = (products) => {
    let len = products.length
    if (len === 0) {
        getElement('myTabContent').innerHTML = `<div class="">
            <h1 class="text-light text-center">Coming Soon! o(TヘTo)</h1>
        </div>`
        getElement('myTab').innerHTML = ''
    } else {
        renderTabPane(len, products)
        renderNavItem(len)
    }

}

export const renderBrand = (brands) => {
    let strBrand = '<option selected value="">Chọn hãng</option>'
    brands.map(item => {
        let { brand } = item
        strBrand += `
        <option value='${brand}'>${brand.charAt(0).toUpperCase() + brand.slice(1)}</option>
    `
    })
    getElement('brand').innerHTML = strBrand
}

export const renderCart = (carts) => {
    let strHTML = '', sumToTal = 0, sumQty = 0;
    if (carts.length === 0) {
        getElement('purchase').disabled = true
        strHTML = `
            <tr>
                <td colspan='4' class='display-6 p-5'>Mua gì ủng hộ đi bạn ơi o((>ω< ))o</td>
            </tr>
        `
    } else {
        getElement('purchase').disabled = false
        carts.map((cart, index) => {
            let { name, img, price, qty } = cart
            strHTML += `
                <tr>
                    <td><img style='width: 50px; height: 50px' src='${img}'></td>
                    <td>${name}</td>
                    <td>
                        <button onclick='changeQty(${index},-1)' class='btn btn-light'>
                            <i class="fa fa-angle-left"></i>
                        </button>
                            ${qty}
                        <button onclick='changeQty(${index},1)' class='btn btn-light'>
                            <i class="fa fa-angle-right"></i>
                        </button>
                    </td>
                    <td>${(qty * price).toLocaleString()} vnđ</td>
                    <td><button onclick='removeCart(${index})' class='text-light'><i class="fa fa-trash"></i></button></td>
                </tr>
            `
            sumToTal += qty * price;
            sumQty += qty
        })
    }
    getElement('total').innerHTML = sumToTal.toLocaleString() + ' vnđ'
    getElement('qty').innerHTML = sumQty
    getElement('body-cart').innerHTML = strHTML
}

export const renderOrder = (carts, status) => {
    let strOrder = '', sum = 0;
    if (status === 0) {
        getElement('modal-dialog').className = 'modal-dialog';
        getElement('order').style.display = 'block'
        carts.map(cart => {
            let { name, price, qty } = cart
            strOrder += `
                <div class='item-order'>
                    <span>${qty + ' x ' + name}</span>
                    <span>${(qty * price).toLocaleString()} vnđ</span>
                </div>
            `
            sum += qty * price
        })

        strOrder += `
            <div class='hr item-order'>
                <span>Tổng cộng</span>
                <span>${sum.toLocaleString()} vnđ</span>
            </div>
        `
        getElement('modal-body').innerHTML = strOrder
    } else {
        getElement('order').style.display = 'none'
        getElement('modal-dialog').className = 'modal-dialog report';
        getElement('modal-body').innerHTML = "Cảm ơn đã mua hàng (╹ڡ╹ )"
    }
}

export const renderTblProduct = (products) => {
    let strTable = ''
    products.map(product => {
        let { id,name,price,img,desc,type } = product
        strTable += `
            <tr>
                <td>${id}</td>
                <td>${name}</td>
                <td>${type}</td>
                <td>${(price*1).toLocaleString()} vnd</td>
                <td><img src='${img}' style="width: 50px; height: 50px"></td>
                <td>${desc}</td>
                <td>
                    <button onclick='deleteProduct("${id}")' class='btn btn-danger'>Xóa</button>
                    <button onclick='getProduct("${id}")' class='btn btn-primary'>Chọn</button>
                </td>
            </tr>
        `
    })
    getElement('tbodyProduct').innerHTML = strTable
}