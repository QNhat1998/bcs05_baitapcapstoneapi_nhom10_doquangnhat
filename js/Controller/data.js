export const getData = (url) => {
    return axios.get(url)
}

export const getID = (url,id) => {
    console.log(`${url}/${id}`)
    return axios.get(`${url}/${id}`)
}

export const deleteData = (url, id) => {
    return axios({
        url: `${url}/${id}`,
        method: 'DELETE'
    })
}

export const postData = (url, data) => {
    return axios({
        url,
        method: 'POST',
        data
    })
}

export const putData = (url,id, data) => {
    return axios({
        url: `${url}/${id}`,
        method: 'PUT', 
        data
    })
}






