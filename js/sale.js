import { renderProduct, renderBrand, renderOrder, renderCart } from './Controller/render.js'
import { getData } from './Controller/data.js';
import CartList from './Model/CartList.js';
import Cart from './Model/Cart.js';

const urlProduct = 'https://635d3a2afc2595be2656024b.mockapi.io/apis/products',
    urlBrand = 'https://635d3a2afc2595be2656024b.mockapi.io/apis/type';
let plist = [],
    clist = new CartList()
// Promise.all([getData(urlProduct), getData(urlBrand)]).then(res => {
//     plist.productList = res[0],
//     renderProduct(plist.productList)
//     renderBrand(res[1])
// })
getData(urlProduct).then(res=>{
    plist = res.data
    renderProduct(plist)
})
getData(urlBrand).then(res=>{
    renderBrand(res.data)
})
const sideCart = (value) => {
    let overlay = document.querySelector('.overlay'),
        side = document.querySelector('.side-cart');
    (side.style.right = value ? '0' : '-100%'),
        (overlay.style.display = value ? 'block' : 'none')
}
const changeValue = (event) => {
    let value = event.target.value;
    let newPList = []

    if (value !== '') {
        newPList = plist.filter(product => product.type.toLowerCase() === value)
        renderProduct(newPList)
    } else {
        renderProduct(plist)
    }
}
const addCart = (id) => {
    let indexP = plist.findIndex(p => p.id === id),
        indexC = clist.cartList.findIndex(cart => cart.id === id)
    if (indexC === -1) {
        let { name, price, img } = plist[indexP],
            newCart = new Cart(id, name, price, img, 1);
        clist.cartList.push(newCart)

    } else {
        clist.cartList[indexC].qty += 1
    }
    renderCart(clist.cartList)
    setStorage()
}
const removeCart = (index, isAll = 0) => {
    clist.removeCart(index, isAll)
    setStorage()
    renderCart(clist.cartList)
}
const changeQty = (index, value) => {
    clist.addQty(index, value)
    setStorage()
    renderCart(clist.cartList)
}
const showModalOrder = () => {
    renderOrder(clist.cartList, 0)
}
const setStorage = () => {
    let jsCartList = JSON.stringify(clist.cartList)
    localStorage.setItem("CartList", jsCartList)
}
const getStorage = () => {
    let jsCartList = localStorage.getItem("CartList")
    if (jsCartList) {
        var arrCarts = JSON.parse(jsCartList);
        clist.cartList = arrCarts;
        renderCart(clist.cartList)
    }
}
const purchase = () => {
    removeCart(0, 1)
    renderOrder(clist.cartList, 1)
}
getStorage()
window.sideCart = sideCart
window.changeValue = changeValue
window.addCart = addCart
window.showModalOrder = showModalOrder
window.removeCart = removeCart
window.changeQty = changeQty
window.purchase = purchase