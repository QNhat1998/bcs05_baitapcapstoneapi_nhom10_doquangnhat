import { getData, deleteData, getID, postData, putData } from "./Controller/data.js"
import { renderBrand, renderTblProduct } from "./Controller/render.js";
import { assignValue, getValue } from "./Controller/domValue.js";
import Product from "./Model/Product.js";
import Validation from "./Controller/Validation.js";

const urlProduct = 'https://635d3a2afc2595be2656024b.mockapi.io/apis/products',
    urlBrand = 'https://635d3a2afc2595be2656024b.mockapi.io/apis/type';

let plist = [],
    val = new Validation(),
    getId

const getElement = id => document.getElementById(id)

// Promise.all([getData(urlProduct), getData(urlBrand)]).then(res => {
//     plist = res[0]
//     len = plist.length
//     renderTblProduct(plist)
//     renderBrand(res[1])
// })

getData(urlBrand).then(res => {
    renderBrand(res.data)
})

const fetchData = () => {
    getData(urlProduct).then(res => {
        plist = res.data
        renderTblProduct(plist)
    })
}

const checkValid = (add = 0) => {
    let { name, brand, price, img, desc } = getValue(),
        valid = true
    valid &= val.kiemTraRong(name, 'tbTen') && val.kiemTraChuVaSo(name, 'tbTen') && (add === 1 ? val.kiemTrTonTai(name, plist, 'tbTen') : true)
    valid &= val.kiemTraRong(brand, 'tbLoai')
    valid &= val.kiemTraRong(price, 'tbGia')
        && val.kiemTraSo(price, 'tbGia')
        && val.kiemTraGiaTri(price, 'tbGia', 500000, 100000000)
    valid &= val.kiemTraRong(img, 'tbHinhAnh') &&
        val.kiemTraDuongDan(img, 'tbHinhAnh')
    valid &= val.kiemTraRong(desc, 'tbMoTa')
    return valid
}

const showImg = (event) => {
    let value = event.target.value.trim()
    let link = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/
    if (link.test(value)) {
        getElement('show').setAttribute('src', `${value}`)
        getElement('show').style.display = 'block ';
    } else {
        getElement('show').style.display = 'none ';
    }
}

const disappear = (text) => {
    let tacvu = getElement('tbTacVu')
    tacvu.style.opacity = '1'
    tacvu.innerHTML = text
    setTimeout(() => {
        tacvu.style.opacity = '0'
    }, 3000)
}

const statusBtn = (is) => {
    getElement('update').disabled = !is
    getElement('add').disabled = is
}

const clearForm = () => {
    assignValue();
    statusBtn(false)
    getElement('show').style.display = 'none ';
}

const getProduct = (id) => {
    getID(urlProduct, id).then(res => {
        let { id, name, type, price, img, desc } = res.data
        getId = id
        assignValue(name, type, price, img, desc)
        statusBtn(true)
    }).catch(err => {
        console.log(err)
    })
}

const deleteProduct = id => {
    deleteData(urlProduct, id).then(res => {
        disappear(`Xóa thành công`)
        fetchData()
    }).catch(err => {
        console.log(err)
    })
}

const addProduct = () => {
    let { name, brand, price, img, desc } = getValue()
    console.log(checkValid(1))
    if (checkValid(1)) {
        let product = new Product(name, price, img, desc, brand)
        postData(urlProduct, product).then(res => {
            console.log(res)
            disappear(`Thêm ${name} thành công`)
            clearForm()
            fetchData()
        }).catch(err => {
            console.log(err)
        })
    }
}

const updateProduct = () => {
    if (checkValid()) {
        let { name, brand, price, img, desc } = getValue()
        let product = new Product(name, price, img, desc, brand)
        putData(urlProduct, getId, product).then(res => {
            disappear(`Sửa ${name} thành công`)
            fetchData()
            clearForm()
        }).catch(err => {
            console.log(err)
        })
    }
}

getElement('update').disabled = true
fetchData()

window.deleteProduct = deleteProduct
window.updateProduct = updateProduct
window.addProduct = addProduct
window.getProduct = getProduct
window.clearForm = clearForm
window.showImg = showImg


