class Product {
  constructor(
    name,
    price,
    img,
    desc,
    type
  ) {
    (this.name = name),
      (this.price = price),
      (this.img = img),
      (this.desc = desc),
      (this.type = type);
  }
}

export default Product;