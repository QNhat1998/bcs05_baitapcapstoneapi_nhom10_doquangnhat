class Cart {
    constructor(id,name,price,img,qty = 1){
        this.id = id,
        this.name = name,
        this.price = price,
        this.img = img,
        this.qty = qty
    }
}

export default Cart