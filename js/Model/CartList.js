class CartList {
    constructor(){
        this.cartList = []
    }

    addCart(cart){
        this.cartList.push(cart)
    }
    removeCart(index,isAll){
        if(isAll){
            this.cartList.splice(index,this.cartList.length)
        }else{
            this.cartList.splice(index,1)
        }
    }
    addQty(index,value){
        if(this.cartList[index].qty === 1 && value === -1)
            this.removeCart(index)
        else
            this.cartList[index].qty += value;
    }
}

export default CartList